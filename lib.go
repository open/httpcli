package httpcli

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
)

type Cli struct {
	cli      *http.Client
	headers  map[string]string
	basePath string
}

func (c *Cli) SetCli(h *http.Client) {
	c.cli = h
}
func (c *Cli) BasePath() string {
	return c.basePath
}
func (c *Cli) SetBasePath(basePath string) {
	c.basePath = basePath
}
func (c *Cli) AddHeader(k string, v string) {
	c.headers[k] = v
}
func (c *Cli) DelHeader(k string) {
	delete(c.headers, k)
}
func (c *Cli) Do(method string, strurl string, body []byte) (*http.Response, error) {
	if !strings.HasPrefix(strurl, "http") {
		addr := c.BasePath()
		if strings.HasSuffix(addr, "/") {
			addr = addr[:len(addr)-1]
		}
		if !strings.HasPrefix(strurl, "/") {
			strurl = "/" + strurl
		}
		strurl = addr + strurl
	}
	var req *http.Request
	var err error

	if body != nil && len(body) > 0 {
		req, err = http.NewRequest(method, strurl, io.NopCloser(bytes.NewReader(body)))

	} else {
		req, err = http.NewRequest(method, strurl, nil)
	}
	if err != nil {
		return nil, err
	}
	if c.headers != nil {
		for k, v := range c.headers {
			req.Header.Set(k, v)
		}
	}
	res, err := c.cli.Do(req)
	if err != nil {
		return nil, err
	}
	if res.StatusCode >= 400 {
		return nil, errors.New(fmt.Sprintf("Http return code - %d: %s", res.StatusCode, res.Status))
	}
	return res, err
}
func (c *Cli) DoJson(method string, strurl string, i interface{}, o interface{}) (err error) {
	bs, err := json.Marshal(i)
	if err != nil {
		return err
	}
	res, err := c.Do(method, strurl, bs)
	if err != nil {
		return
	}
	defer res.Body.Close()
	if o != nil {
		err = json.NewDecoder(res.Body).Decode(o)
	}
	return
}
func (c *Cli) JsonGet(strurl string, o interface{}) error {
	return c.DoJson(http.MethodGet, strurl, nil, o)
}
func (c *Cli) JsonDelete(strurl string, o interface{}) error {
	return c.DoJson(http.MethodDelete, strurl, nil, o)
}
func (c *Cli) JsonHead(strurl string, o interface{}) error {
	return c.DoJson(http.MethodHead, strurl, nil, o)
}
func (c *Cli) JsonPost(strurl string, i interface{}, o interface{}) error {
	return c.DoJson(http.MethodPost, strurl, i, o)
}
func (c *Cli) JsonPut(strurl string, i interface{}, o interface{}) error {
	return c.DoJson(http.MethodPut, strurl, i, o)
}
func (c *Cli) JsonPatch(strurl string, i interface{}, o interface{}) error {
	return c.DoJson(http.MethodPatch, strurl, i, o)
}
func (c *Cli) RawGet(strurl string) ([]byte, error) {
	res, err := c.Do(http.MethodGet, strurl, nil)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	bs, _ := io.ReadAll(res.Body)
	return bs, nil
}
func (c *Cli) RawDelete(strurl string) ([]byte, error) {
	res, err := c.Do(http.MethodDelete, strurl, nil)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	bs, _ := io.ReadAll(res.Body)
	return bs, nil
}
func (c *Cli) RawPost(strurl string, i []byte) ([]byte, error) {
	res, err := c.Do(http.MethodPost, strurl, i)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	bs, _ := io.ReadAll(res.Body)
	return bs, nil
}
func (c *Cli) RawPut(strurl string, i []byte) ([]byte, error) {
	res, err := c.Do(http.MethodPut, strurl, i)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	bs, _ := io.ReadAll(res.Body)
	return bs, nil
}

func NewCli() *Cli {
	ret := &Cli{
		cli:     &http.Client{},
		headers: make(map[string]string),
	}
	return ret
}

var cli *Cli

func C() *Cli {
	if cli == nil {
		cli = NewCli()
	}
	return cli
}
